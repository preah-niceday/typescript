/** @transform {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    globals: {},
    moduleNameMapper : {
        '@/(.*)$': '<rootDir>/src/$1',
        '#/(.*)$': '<rootDir>/tests/$1',
    },
    // transform: {
    //     '\\.js$': ['babel-jest', { configFile: './babel-jest.config.js' }],
    // },
    // transform: {
    //     "^.+\\.js": "<rootDir>/node_modules/babel-jest",
    // },
    // moduleFileExtensions: ["vue", "js", "json", "jsx", "ts", "tsx", "node"],
    setupFilesAfterEnv: ['./jest.setup.js']
};