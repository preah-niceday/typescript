import {Mapper, NewInstance} from "@/helper/object.helper";
import {Model} from "@/model/DateType.model";
import {LocalDate, LocalDateTime, LocalTime} from "@js-joda/core";


describe("파라메터", () => {

  const mapper:Mapper = new Mapper();
  const newInstance:NewInstance = new NewInstance();

  test("json to model", () => {
    const jsonObject = JSON.parse('{"time":"09:42", "date":"2016-12-24", "dateTime": "2016-02-26T09:42:42.123", "times":["09:42","09:42"]}');
    const objectToModel:Model.Date = mapper.toClass(Model.Date, jsonObject);
    expect(objectToModel.time).not.toBeNull();
    expect(objectToModel.date).not.toBeNull();
    expect(objectToModel.dateTime).not.toBeNull();

    objectToModel.time = LocalTime.now();
    console.log(JSON.stringify(objectToModel));
    console.log(objectToModel);
  });


  test("model to model", () => {

    const one:Model.Date = new Model.Date();
    one.time = LocalTime.parse("20:40");
    one.date = LocalDate.parse("2022-08-01");
    one.dateTime = LocalDateTime.parse("2022-08-01T20:42:42.123");

    one.times = [LocalTime.parse("20:40"), LocalTime.parse("19:40")];
    one.dates = [LocalDate.parse("2022-08-01"), LocalDate.parse("2022-07-01")];
    one.dateTimes = [LocalDateTime.parse("2022-08-01T20:42:42.123"), LocalDateTime.parse("2022-07-01T20:42:42.123")];

    const two = mapper.toClass(Model.Date, one);
    expect(two.time).not.toBeNull();
    expect(two.date).not.toBeNull();
    expect(two.dateTime).not.toBeNull();
    console.log(two);
  });


  test("object to model", () => {

    const object:any = {};
    object.time = LocalTime.parse("20:40");
    object.date = LocalDate.parse("2022-08-01");
    object.dateTime = LocalDateTime.parse("2022-08-01T20:42:42.123");

    const objectToModel = mapper.toClass(Model.Date, object);
    expect(objectToModel.time).not.toBeNull();
    expect(objectToModel.date).not.toBeNull();
    expect(objectToModel.dateTime).not.toBeNull();
    console.log(objectToModel);
  });


  test("date format", () => {

    const object:any = JSON.parse('{"time":"0942", "date":"20161224", "dateTime":"20220701204242", "times":["0942"], "dates":["20161224"], "dateTimes":["20220701204242"]}');
    const objectToModel = mapper.toClass(Model.DatePattern, object);
    console.log(objectToModel);
  });


  test("object to date model", ()=> {

    const model:Model.Date = newInstance.create(Model.Date);
    console.log(model);
  });
})

