import "reflect-metadata";
import {Mapper, NewInstance} from "../helper/object.helper";
import {Model} from "../model/EnumType.model";
import {ComponentCode} from "../model/enumerate/ComponentCode";

describe("이넘", () => {

  const mapper:Mapper = new Mapper();
  const newInstance:NewInstance = new NewInstance();

  test("code", ()=> {

    const model:Model.Enum = new Model.Enum();

    model.componentCode = ComponentCode.IMAGE;

    console.log(model.componentCode.value);
    console.log(model.componentCode.name);

    expect(model.componentCode).not.toEqual(ComponentCode.TEXT);
    expect(model.componentCode).not.toEqual(ComponentCode.BOARD);
    expect(model.componentCode).toEqual(ComponentCode.IMAGE);
    expect(model.componentCode.name).toEqual(ComponentCode.IMAGE.name);
    expect(model.componentCode.value).toEqual(ComponentCode.IMAGE.value);

    switch (model.componentCode) {
      case ComponentCode.TEXT  : expect(model.componentCode).toEqual(ComponentCode.TEXT);  break;
      case ComponentCode.IMAGE : expect(model.componentCode).toEqual(ComponentCode.IMAGE); break;
      case ComponentCode.BOARD : expect(model.componentCode).toEqual(ComponentCode.BOARD); break;
      default : throw new Error('not valid');
    }
  });

  test("json to model", ()=> {

    const jsonObject = JSON.parse('{"componentCode":"BOARD", "componentCodes":["BOARD", "IMAGE"]}');
    const objectToModel = mapper.toClass(Model.Enum, jsonObject);
    debugger
    console.log(objectToModel);
  });

  test("model to model", ()=> {

    const one:Model.Enum = new Model.Enum();
    one.componentCode = ComponentCode.IMAGE;
    one.componentCodes = [ComponentCode.IMAGE, ComponentCode.BOARD];
    const two:Model.Enum = mapper.toClass(Model.Enum, one);
    console.log(two);
  });

  test("object to model", ()=> {

    const model:Model.Enum = newInstance.create(Model.Enum);
    console.log(model);
  });

  test("enum default", ()=> {
    console.log(ComponentCode.IMAGE.value);
    console.log(ComponentCode.IMAGE.name);
  });
});;
