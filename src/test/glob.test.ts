import "reflect-metadata";
import {Mapper, NewInstance} from "../helper/object.helper";
import {glob} from "glob";

describe("glob", ()=> {

  const mapper:Mapper = new Mapper();
  const newInstance:NewInstance = new NewInstance();

  const recursive = (target:any) => {

      for(const key of Object.keys(target)) {
        const item = target[key];
        switch (typeof item) {
          case "object"  : recursive(item); break;
          case "function": isFunction(item); break;
          default : break;
        }
      }
    }

  const isFunction = (target:any) => {

      const model = new target();
      for(const propertyName of Object.keys(model)) {

        const designType = Reflect.getMetadata('design:transform', model, propertyName);
        console.log(designType);
        debugger
      }

      console.log(target.constructor());

      debugger
    }

  test("find target", async () => {

    const find = new glob.GlobSync("src/**/**.model.ts", {});
    console.log(find.found);
    const target = await import(find.found[0].replace("src", ".."));

    recursive(target);

    debugger
  });
});
