import "reflect-metadata";

import {Model} from "../model/NestedType.model";
import {container} from "tsyringe";
import {Mapper, NewInstance} from "../helper/object.helper";
import {ComponentCode} from "../model/enumerate/ComponentCode";
import {MakeCode} from "../model/enumerate/MakeCode";
import * as console from "console";


describe("오브젝트", () => {

  const mapper:Mapper = container.resolve(Mapper);
  const instance:NewInstance = container.resolve(NewInstance);

  test("object assign", ()=> {
    const sampleObject = instance.create(Model.Request.Add);
    const jsonString = JSON.stringify(sampleObject);
    const jsonObject = JSON.parse(jsonString);

    // const add = new Model.Request.Add();
    // add.makeCodes = [MakeCode.COMPLETE];
    // add.makeCode = MakeCode.DIGEST;
    // // debugger
    let sample1:Model.Request.Add = mapper.toClass(Model.Request.Add, jsonObject);
    // console.log(add);
    // let sample2:Model.Request.Add = mapper.toClass(Model.Request.Add, add);
    console.log(sample1);
    // console.log(sample2);
    // console.log(jsonObject);
  });

  test("object attribute", ()=> {
    const findAll:Model.Response.FindAll = new Model.Response.FindAll();
    findAll.name = "name";
    findAll.target = instance.create(Model.Response.Target);
    findAll.targets = [instance.create(Model.Response.Target), instance.create(Model.Response.Target)];
    findAll.componentCode = ComponentCode.IMAGE;
    findAll.componentCodes = [ComponentCode.IMAGE, ComponentCode.TEXT];

    const jsonObject = JSON.parse(JSON.stringify(findAll));
    let sample:Model.Response.FindAll = mapper.toClass(Model.Response.FindAll, jsonObject);
    debugger
    console.log(sample);
  });

  test("object deep model", ()=> {
    const model:Model.Request.Add = instance.create(Model.Request.Add);
    // model.makeCode = MakeCode.COMPLETE;
    const jsonObject = JSON.parse(JSON.stringify(model));
    debugger
    let sample:Model.Request.Add = mapper.toClass(Model.Request.Add, jsonObject);
    console.log(sample);
  });

  test("object to json", ()=> {
    const model:Model.Enum = instance.create(Model.Enum);
    console.log(model);
    const jsonString = JSON.stringify(model);
    console.log(jsonString);
  });


  test("new mode", () => {
    const model:Model.Request.Add = instance.create(Model.Request.Add);
    // const model:Model.Request.Add = new Model.Request.Add();
    const obj = JSON.parse(JSON.stringify('{"displayYn":"true"}'));
    const sample = mapper.toClass(Model.Request.Add, obj);
    console.log(sample);
    // debugger
  });


  test("object to class model", ()=> {

    const model:Model.Request.Add = instance.create(Model.Request.Add);
    console.log(model);
  });
});
