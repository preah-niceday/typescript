import "reflect-metadata";

import {Model} from "../model/NestedType.model";
import {Field} from "../model/base/field.base";


describe("설명", ()=> {
  test("", ()=> {
    let add:Model.Request.Add = new Model.Request.Add;
    const fields:Field[] = Reflect.getMetadata("_fields", add.constructor);

    fields.forEach(field => {
      console.log(`name : ${field.name} / description : ${field.description}`);
    });
  });
});
