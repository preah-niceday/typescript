import {ClassAopDecorator} from "@/decorator/class-aop.decorator";

@ClassAopDecorator()
class Calculator {
  plus(a:number, b:number) {
    return a + b;
  }

  async minus(a:number, b:number) {
    return await Promise.resolve({a:a, b:b}).then((value) => value.a - value.b);
  }
}

describe("class - aop", () => {

  test("plus", ()=> {
    const calculator:Calculator = new Calculator();

    expect(calculator.plus(1, 1)).toBe(2);
  });

  test("minus", async ()=> {
    const calculator:Calculator = new Calculator();
    expect(await calculator.minus(1, 1)).toBe(0);
  });

});

