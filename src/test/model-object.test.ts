import "reflect-metadata";

import {Model} from "../model/NestedType.model";
import {container} from "tsyringe";
import {Mapper, NewInstance} from "../helper/object.helper";
import * as console from "console";
import {LocalDateTime, LocalTime} from "@js-joda/core";


describe("모델 - 오브젝트", () => {

  const mapper:Mapper = container.resolve(Mapper);
  const instance:NewInstance = container.resolve(NewInstance);

  test("1. 백엔드에서 전달해준 json 형식의 결과 값을 모델로 변환", ()=> {

    const json  :string =
    `{
        "createdAt"  : "2024-04-26T16:55:41.674",
        "description": "description",
        "displayYn"  : true,
        "image": {
            "displayYn": true,
            "name"     : "name"
        },
        "images": [
            {
                "displayYn": true,
                "name"     : "name"
            }
        ],
        "keyword"  : "keyword",
        "makeCode" : "DIGEST",
        "makeCodes": ["DIGEST", "COMPLETE", "EXTRACT"],
        "name"     : "name",
        "serial"   : 0,
        "serials"  : [0, 1, 2],
        "status"   : [true, false, true],
        "templates": [
            {
                "displayYn": true,
                "endDt"    : "2024-04-26",
                "name"     : "name",
                "pages"    : [
                    {
                        "bodies": [
                            {
                                "componentCode": "TEXT",
                                "displayYn"    : true,
                                "imagePool"    : {
                                    "displayYn": true,
                                    "images"   : [
                                        {
                                            "displayYn": true,
                                            "name"     : "name"
                                        }
                                    ],
                                    "name": "name"
                                },
                                "name": "name"
                            }
                        ],
                        "displayYn": true,
                        "name"     : "name"
                    }
                ],
                "startDt": "2024-04-26"
            }
        ],
        "titles"   : ["A", "B", "C"],
        "updatedAt": "2024-04-26T16:55:41.674",
        "url"      : "url",
        "versions" : [
            {
                "displayYn": true,
                "endDt"    : "2024-04-26T16:55:41.674",
                "name"     : "name",
                "startDt"  : "2024-04-26T16:55:41.672"
            }
        ]
    }`;
    const object:object = JSON.parse(json);

    const add  = mapper.toClass(Model.Request.Add, object);

    expect(add.constructor).toEqual(new Model.Request.Add().constructor);

    console.log(object);
    console.log(add);
    const plant = mapper.toPlain(add);
    console.log(plant)

    debugger
  });

  test("2. 동일 모델 객체의 변환", ()=> {

    const findAll = instance.create(Model.Response.FindAll);


    const assign = mapper.toClass(Model.Response.FindAll, findAll);
    assign.name = "new find all";

    console.log(findAll);
    console.log(assign);

    debugger
  });

  test("3. 다른 모델 간의 객체 변환", ()=> {

    const findOne = instance.create(Model.Response.FindOne);
    const add = mapper.toClass(Model.Request.Add, findOne as any);

    console.log(findOne);
    console.log(add);

    debugger
  });

  test("4. 프론트에서 사용한 모델을 백엔드로 전달", ()=> {

    const modify = instance.create(Model.Request.Modify);
    debugger

  });

  test("5. 빈 값일 경우 요소 제거", ()=> {

    const modify = instance.create(Model.Request.Modify);
    const query = mapper.toQuery(modify);
    debugger
    console.log(query);

  });

  test("6. 편의 함수", ()=> {
    const clazz = instance.create(Model.Sample);
    // console.log(clazz);
    const plain = mapper.toPlain(clazz);
    console.log(plain);
  });

});

// export class PlantTest {
//
//   @ToPlain()
//   public toClass(param:any) {
//     debugger
//     console.log(param);
//     debugger
//   }
// }