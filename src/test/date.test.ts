import "reflect-metadata";
import {Mapper} from "../helper/object.helper";
import {Model} from "../model/DateType.model";
import {LocalDate, LocalDateTime, LocalTime, ZoneId} from "@js-joda/core";
import '@js-joda/timezone'
import * as console from "console";

describe("일자", () => {
  const mapper:Mapper = new Mapper();

  test("json to model", ()=> {

    const jsonObject = JSON.parse('{"time":"09:42", "date":"2016-12-24", "dateTime": "2016-02-26T09:42:42.123", "times":["09:42","09:42"]}');
    const objectToModel:Model.Date = mapper.toClass(Model.Date, jsonObject);
    expect(objectToModel.time).not.toBeNull();
    expect(objectToModel.date).not.toBeNull();
    expect(objectToModel.dateTime).not.toBeNull();

    objectToModel.time = LocalTime.now();
    console.log(JSON.stringify(objectToModel));
    console.log(objectToModel);
  });

  test("model to model", ()=> {

    const one:Model.Date = new Model.Date();
    one.time = LocalTime.parse("20:40");
    one.date = LocalDate.parse("2022-08-01");
    one.dateTime = LocalDateTime.parse("2022-08-01T20:42:42.123");

    one.times = [LocalTime.parse("20:40"), LocalTime.parse("19:40")];
    one.dates = [LocalDate.parse("2022-08-01"), LocalDate.parse("2022-07-01")];
    one.dateTimes = [LocalDateTime.parse("2022-08-01T20:42:42.123"), LocalDateTime.parse("2022-07-01T20:42:42.123")];

    const two = mapper.toClass(Model.Date, one);
    expect(two.time).not.toBeNull();
    expect(two.date).not.toBeNull();
    expect(two.dateTime).not.toBeNull();
    console.log(two);
  });

  test("object to model", ()=> {

    const object:any = {};
    object.time = LocalTime.parse("20:40");
    object.date = LocalDate.parse("2022-08-01");
    object.dateTime = LocalDateTime.parse("2022-08-01T20:42:42.123");


    debugger


    const objectToModel = mapper.toClass(Model.Date, object);
    expect(objectToModel.time).not.toBeNull();
    expect(objectToModel.date).not.toBeNull();
    expect(objectToModel.dateTime).not.toBeNull();
    console.log(objectToModel);
  });

  test("date format", ()=> {

    const object:any = JSON.parse('{"time":"0942", "date":"20161224", "dateTime":"20220701204242", "times":["0942"], "dates":["20161224"], "dateTimes":["20220701204242"]}');
    const objectToModel = mapper.toClass(Model.DatePattern, object);
    console.log(objectToModel);
  });


  test("date", () => {
    const date:LocalDate = LocalDate.now();

    const day = LocalDate.of(2023, 12, 7);
    console.log(day.toString());

    console.log(date.toString());
    console.log(date.year());
    console.log(date.monthValue());
    console.log(date.dayOfMonth());
  });

  test("time zone", async () => {

    const now:LocalDateTime = LocalDateTime.now().atZone(ZoneId.UTC).toLocalDateTime();
    // console.log("UTC : " + now.atZone(ZoneId.UTC).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    // console.log("Asia/Bangkok : " + now.atOffset(ZoneId.of("Asia/Bangkok")).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).toString());


    console.log(ZoneId.of("Asia/Bangkok"));

    const day:LocalDateTime = LocalDateTime.parse("2023-12-08T00:00:00.000");

    console.log(day.plusHours(new Date().getTimezoneOffset() / 60).toString());
    console.log(`Time difference between ${Intl.DateTimeFormat().resolvedOptions().timeZone} and UTC: ${new Date().getTimezoneOffset() / 60} hours`);



    // console.log(day.atZone(ZoneId.of("Asia/Bangkok")).toOffsetDateTime().toLocalDateTime().toString());
    //
    //
    // console.log(DateTimeFormatter.ISO_INSTANT.format(day.atZone(ZoneId.of("Asia/Bangkok")).toOffsetDateTime()));
    // console.log(ZoneOffset.getAvailableZoneIds());

    // console.log(Instant.now())
// Get the user's local time zone

// // Get the current time in the user's local time zone
//     const localTime = new Date();
//     const localTimeZoneOffset = localTime.getTimezoneOffset() / 60;
// // Output the time difference
//     console.log(`Time difference between ${userTimeZone} and UTC: ${localTimeZoneOffset} hours`);


    // console.log("SYSTEM : " + now.toString());
    // console.log("Asia/Seoul : " + now.atZone(ZoneId.of("Asia/Seoul")).toString());
    // console.log("Asia/Bangkok : " + now.atZone(ZoneId.of("Asia/Bangkok")).toString());



    // console.log("\n\n");
    // for(const zoneId of ZoneId.getAvailableZoneIds()) {
    //   console.log(zoneId + " : " + now.atZone(ZoneId.of(zoneId)).toString());
    // }

  });
});
