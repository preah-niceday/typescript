import {Mapper, NewInstance} from "@/helper/object.helper";
import {container} from "tsyringe";
import {Model} from "@/model/NestedType.model";


describe("샘플값 생성", () => {

    const mapper:Mapper = container.resolve(Mapper);
    const instance:NewInstance = container.resolve(NewInstance);

    test("nested sample data init-1", ()=> {
        const add:Model.Request.Add = instance.create(Model.Request.Add);
        console.log(add);
    });

    test("nested sample data init-2", ()=> {
        const date:Model.Date = instance.create(Model.Date);
        console.log(date);
    });
});
