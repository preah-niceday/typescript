import {BoardEnumerate} from "./attribute/Board-Enumerate";
import {ComponentEnumerate} from "./attribute/Component-Enumerate";

export namespace Enumerate {

    export import Board = BoardEnumerate;

    export namespace Image {
        export enum ImageCode {
            VERTICAL = '수직',
            HORIZONTAL = '수평'
        }

        export enum DisplayCode {
            PUBLIC = '이미지-일반',
            PRIVATE = '이미지-비공개'
        }
    }

    export namespace Audio {
        export namespace Component {
            export import Book = ComponentEnumerate.Audio.Book;
        }
    }
}