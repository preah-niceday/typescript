export namespace ComponentEnumerate {
    export namespace Audio {
        export namespace Book {
            export enum MakeCode {
                DIGEST   = "요약형",
                COMPLETE = "완독형",
                EXTRACT  = "발췌형"
            }
            export enum LanguageCode {
                KOREA   = "한국어",
                ENGLISH = "영어",
                OTHER   = "기타"
            }
        }
    }
}