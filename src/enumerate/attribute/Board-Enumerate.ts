export namespace BoardEnumerate {
    export enum BoardCode {
        TEXT  = '텍스트',
        IMAGE = '이미지'
    }
    export enum DisplayCode {
        PUBLIC  = '게시판-일반',
        PRIVATE = '게시판-비공개'
    }
}
