import "reflect-metadata";
// @ts-ignore
import {defaultMetadataStorage} from "class-transformer/cjs/storage.js";
import {ClassConstructor, ClassTransformer, instanceToPlain, plainToInstance} from "class-transformer";
import {singleton} from "tsyringe";
import {Pageable} from "../model/base/pageable.base";
import _ from "lodash";
import {LocalDate, LocalDateTime, LocalTime} from "@js-joda/core";
import {NEnumerate} from "@/model/transform/n-enumerate";

const metadataStorage:defaultMetadataStorage = defaultMetadataStorage;

@singleton()
export class NewInstance {

    private typeMetas  :any = Reflect.get(metadataStorage, "_typeMetadatas");
    private exposeMetas:any = Reflect.get(metadataStorage, "_exposeMetadatas");

    public create<T>(clazz: ClassConstructor<T>): T {

        const instance:any = new clazz();
        const expose:any = this.exposeMetas.get(instance.constructor);

        for(const key of expose.keys()) {
            const type:any = this.typeMetas?.get(instance.constructor)?.get(key);
            Reflect.set(instance, key, this.addValue(instance, key, type));
        }
        const extendsExpose = this.exposeMetas.get(Object.getPrototypeOf(instance.constructor));

        if(!_.isUndefined(extendsExpose)) {
            for(const key of extendsExpose.keys()) {
                const type:any = this.typeMetas?.get(Object.getPrototypeOf(instance.constructor))?.get(key);
                Reflect.set(instance, key, this.addValue(instance, key, type));
            }
        }

        return instance;
    }

    private addValue(instance:any, key:string, type:any) : any {

        if(type === undefined) {
            return this.setValue(instance, key);
        }

        switch (type?.reflectedType) {
            case Array         : return this.typeArray(type);
            case String        : return Reflect.ownKeys(type.typeFunction())[0];
            case LocalTime     : return LocalTime.now();
            case LocalDate     : return LocalDate.now();
            case LocalDateTime : return LocalDateTime.now();
            default       : {
                if(_.isEqual(NEnumerate, Object.getPrototypeOf(type.reflectedType))) {
                    return type.reflectedType[Object.keys(type.reflectedType)[0]];
                }
                return this.create((type.typeFunction()));
            }
        }
    }

    private typeArray(type?:any):any {
        switch (type?.typeFunction()) {
            case Number        : return [0,1,2];
            case String        : return ['A','B','C'];
            case Boolean       : return [true,false,true];
            case Date          : return [Date(),Date(),Date()];
            case LocalTime     : return [LocalTime.now(),LocalTime.now(),LocalTime.now()];
            case LocalDate     : return [LocalDate.now(),LocalDate.now(),LocalDate.now()];
            case LocalDateTime : return [LocalDateTime.now(),LocalDateTime.now(),LocalDateTime.now()];
            default        : {
                if(_.isEqual(NEnumerate, Object.getPrototypeOf(type?.typeFunction()))) {
                    const values = [];
                    for(const key of Object.keys(type.typeFunction())) {
                        values.push(type.typeFunction()[key]);
                    }
                    return values;
                }
                return [this.create(type.typeFunction())];
            }
        }
    }

    private setValue(instance:any, key:string): any {
        const attributeType:any = Reflect.getMetadata("design:transform", instance, key);
        switch (attributeType.name) {
            case "String"  : return key;
            case "Boolean" : return true;
            case "Number"  : return 0;
            case "Date"    : return Date();
            default        : return null;
        }
    }
}

@singleton()
export class Mapper extends ClassTransformer {

    private typeMetas  :any = Reflect.get(metadataStorage, "_typeMetadatas");
    private exposeMetas:any = Reflect.get(metadataStorage, "_exposeMetadatas");

    public toClass<T>(type: ClassConstructor<T>, source: T): T {
        return plainToInstance(type, source, {excludeExtraneousValues: true, exposeDefaultValues: true});
    }

    public toArray<T>(type: ClassConstructor<T>, source: T[]): T[] {
        return plainToInstance(type, source, {excludeExtraneousValues: true, exposeDefaultValues: true});
    }

    public toPage<T>(type: ClassConstructor<T>, source: Pageable.Response.Page<T>): Pageable.Response.Page<T> {
        const page = plainToInstance(Pageable.Response.Page, source, {excludeExtraneousValues: true}) as Pageable.Response.Page<T>;
        page.contents = plainToInstance(type, source.contents, {excludeExtraneousValues: true}) as T[];
        return page;
    }

    toPlain<T>(source: T): Record<string, any> {
        return instanceToPlain(source);
    }

    // todo : 계층 수정 필요
    public toQuery<T>(source: any): T {

        for (const key of Reflect.ownKeys(source as any)) {

            const target: any = source[key];
            const designType = Reflect.getMetadata("design:transform", source, key);
            if (_.isUndefined(target) || _.isNull(target)) {
                delete source[key];
            }

            switch (designType.name) {
                case "String" : {
                    if (_.isUndefined(target) || _.isNull(target)) {
                        delete source[key];
                    }
                } break;
                default : {
                    if (_.isUndefined(target) || _.isNull(target)) {
                        delete source[key];
                    }
                }
            }

            if (_.isEqual("NEnumerate", Object.getPrototypeOf(designType.prototype)?.constructor?.name)) {
                source[key] = source[key].value;
            }
        }
        return source;
    }
}
