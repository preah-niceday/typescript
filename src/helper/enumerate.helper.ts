import "reflect-metadata";
import {singleton} from "tsyringe";
import {Enumerate as target} from "../enumerate/Enumerate";

export class Initialize {

    constructor(enumerate:any) {
        this.recursive(enumerate);
    }

    private recursive(enumerate:any) {
        for(const key of Object.keys(enumerate)) {
            switch (typeof enumerate[key]) {
                case "object" : this.recursive(enumerate[key]); break;
                default       : this.execute(enumerate);        break;
            }
        }
    }

    private execute(enumerate : any) {
        if(enumerate.valueOf() instanceof Map) {
            return;
        }

        const attribute = new Map<string, string>();
        const options:Option[] = [];
        for(const key of Object.keys(enumerate)) {
            attribute.set(key, enumerate[key]);
            options.push(new Option(key, enumerate[key]));
            Reflect.set(enumerate, key, key);
        }

        Reflect.defineProperty(enumerate, "valueOf",    {value : () => attribute});
        Reflect.defineProperty(enumerate, "attributes", {value : () => attribute});
        Reflect.defineProperty(enumerate, "options",    {value : () => options});
        Reflect.defineProperty(enumerate, "getText",    {value : (key:any)   => this.getText (key, attribute)});
        Reflect.defineProperty(enumerate, "getValue",   {value : (value:any) => this.getValue(value, attribute)});
    }

    private getText(key : any, attribute: Map<string, string>) {
        return attribute.get(key);
    }

    private getValue(value : any, attribute: Map<string, string>) {
        for(const entity of attribute.entries()) {
            if(entity[1] == value) {
                return entity[0];
            }
        }
        return null;
    }
}

export class Option {
    text! :string;
    value!:string;
    constructor(text:string, value:string) {
        this.text  = text;
        this.value = value;
    }
}

@singleton()
export class Code {

    public enum = target;

    constructor() {
        new Initialize(target);
    }

    public getValue(enumerate : any, key : any):any {
        return (enumerate.valueOf() as Map<string, string>).get(key);
    }
}