import "reflect-metadata";
import {Field} from "../model/base/field.base";
import {Expose, Transform, Type} from "class-transformer";
import {NLocalTime} from "@/model/transform/n-local-time";
import {NLocalDate} from "@/model/transform/n-local-date";
import {NLocalDateTime} from "@/model/transform/n-local-date-time";
import _ from "lodash";
import {NEnumerate} from "@/model/transform/n-enumerate";
import {TransformFnParams} from "class-transformer/types/interfaces";
import {LocalDate, LocalDateTime, LocalTime} from "@js-joda/core";

export function Attribute(description: string, type?:any) {

  return (target: any, propertyName: string) => {

    const fields:Field[] = Reflect.getOwnMetadata('_fields', target.constructor) || [];
    fields.push(new Field(propertyName, description));
    Reflect.defineMetadata("_fields", fields, target.constructor);

    // expose
    Expose({})(target, propertyName);

    const model = new target.constructor();
    const designType = Reflect.getMetadata("design:type", model, propertyName);

    if(_.isUndefined(designType)) console.log("Reflect.getMetadata(design:type) is undefined");

    // primitive type
    switch (designType) {
      case LocalTime : {
        Transform((params: TransformFnParams) => NLocalTime.transformClass(params, target.constructor), {toClassOnly:true})(target, propertyName);
        Transform((params: TransformFnParams) => NLocalTime.transformPlant(params, target.constructor), {toPlainOnly:true})(target, propertyName);
        Type(() => LocalTime, {})(target, propertyName);
      } break;
      case LocalDate : {
        Transform((params: TransformFnParams) => NLocalDate.transformClass(params, target.constructor), {toClassOnly:true})(target, propertyName);
        Transform((params: TransformFnParams) => NLocalDate.transformPlant(params, target.constructor), {toPlainOnly:true})(target, propertyName);
        Type(() => LocalDate, {})(target, propertyName);
      } break;
      case LocalDateTime : {
        Transform((params: TransformFnParams) => NLocalDateTime.transformClass(params, target.constructor), {toClassOnly:true})(target, propertyName);
        Transform((params: TransformFnParams) => NLocalDateTime.transformPlant(params, target.constructor), {toPlainOnly:true})(target, propertyName);
        Type(() => LocalDateTime, {})(target, propertyName);
      } break;
      default : {
        if(_.isEqual(NEnumerate, Object.getPrototypeOf(designType.prototype)?.constructor)){
          Transform((params: TransformFnParams) => NEnumerate.transformClass(params, designType), {toClassOnly:true})(target, propertyName);
          Transform((params: TransformFnParams) => NEnumerate.transformPlant(params, designType), {toPlainOnly:true})(target, propertyName);
          Type(() => designType, {})(target, propertyName);
        }
      } break;
    }

    // type
    if(!_.isUndefined(type)) {

      switch (type()) {
        case LocalTime : {
          Transform((params: TransformFnParams) => NLocalTime.transformClassArray(params, target.constructor), {toClassOnly:true})(target, propertyName);
          Transform((params: TransformFnParams) => NLocalTime.transformPlantArray(params, target.constructor), {toPlainOnly:true})(target, propertyName);
          Type(() => LocalTime, {})(target, propertyName);
        } break;
        case LocalDate : {
          Transform((params: TransformFnParams) => NLocalDate.transformClassArray(params, target.constructor), {toClassOnly:true})(target, propertyName);
          Transform((params: TransformFnParams) => NLocalDate.transformPlantArray(params, target.constructor), {toPlainOnly:true})(target, propertyName);
          Type(() => LocalDate, {})(target, propertyName);
        } break;
        case LocalDateTime : {
          Transform((params: TransformFnParams) => NLocalDateTime.transformClassArray(params, target.constructor), {toClassOnly:true})(target, propertyName);
          Transform((params: TransformFnParams) => NLocalDateTime.transformPlantArray(params, target.constructor), {toPlainOnly:true})(target, propertyName);
          Type(() => LocalDateTime, {})(target, propertyName);
        } break;
        default : {
          Type(type, {})(target, propertyName);
          if(!_.isUndefined(type()?.prototype) && _.isEqual(NEnumerate, Object.getPrototypeOf(type().prototype)?.constructor)){
            Transform((params: TransformFnParams) => NEnumerate.transformClassArray(params, type), {toClassOnly:true})(target, propertyName);
            Transform((params: TransformFnParams) => NEnumerate.transformPlantArray(params, type), {toPlainOnly:true})(target, propertyName);
          }
        } break;
      }
    }
  };
}
