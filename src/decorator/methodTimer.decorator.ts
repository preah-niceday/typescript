export function MethodTimer(logLevel: string) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const originMethod = descriptor.value;
        if (descriptor.value.constructor.name === 'AsyncFunction') {
            descriptor.value = async function (...args:any) {
                const startTime = Date.now();
                const result = await originMethod.apply(this, args);
                const endTime = Date.now();
                console.log(propertyKey, args, endTime - startTime, logLevel);
                return result;
            };
        } else {
            descriptor.value = function (...args:any) {
                const startTime = Date.now();
                const result = originMethod.apply(this, args);
                const endTime = Date.now();
                console.log(propertyKey, args, endTime - startTime, logLevel);
                return result;
            };
        }
    };
}
