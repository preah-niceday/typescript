
export function ClassAopDecorator() {

    return (clazz:any) => {
        const methods = Object.getOwnPropertyNames(clazz.prototype).filter(propertyKey => isNonConstructorMethod(clazz, propertyKey));

        for(const methodName of methods) {

            const descriptor: any = Object.getOwnPropertyDescriptor(clazz.prototype, methodName);
            const originMethod = descriptor!.value;

            switch (descriptor!.value.constructor.name) {
                case "AsyncFunction" : {
                    descriptor!.value = async function (...args: any) {
                        console.log(`AsyncFunction - ${methodName} : start`);
                        const result = await originMethod.apply(this, args);
                        console.log(`AsyncFunction - ${methodName} : end`);
                        return result;
                    }
                } break;
                default : {
                    descriptor!.value = function (...args: any) {
                        console.log(`Function - ${methodName} : start`);
                        const result = originMethod.apply(this, args);
                        console.log(`Function - ${methodName} : end`);
                        return result;
                    };
                }
            }
            Object.defineProperty(clazz.prototype, methodName, descriptor!);
        }
        return clazz;
    }
}

function isNonConstructorMethod(target: any, propertyKey: any) {
    const propertyValue = target.prototype[propertyKey];
    const isMethod = propertyValue instanceof Function;
    return isMethod && propertyKey !== "constructor";
}
