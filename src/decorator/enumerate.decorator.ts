import "reflect-metadata";

export function Enumerate<T extends { new (...args: any[]): {} }>(constructor: any) {
  for(const key of Object.keys(constructor)) {
    const enumerate = constructor[key];
    Reflect.set(enumerate, '_value', key);
  }
}
