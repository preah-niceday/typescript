import "reflect-metadata";
import {Pattern} from "../model/base/pattern.base";

export function DateFormatter(pattern : string) {

  // todo : 패턴 처리 작업 필요
  return (target: any, propertyName: string) => {
    const patterns:Pattern[] = Reflect.getOwnMetadata('_patterns', target.constructor) || [];
    patterns.push(new Pattern(propertyName, pattern));
    Reflect.defineMetadata("_patterns", patterns, target.constructor);
  };
}
