export function ClassTimer() {
    return (clazz: any) => {

        const methods = Object.getOwnPropertyNames(clazz.prototype).filter(propertyKey => isNonConstructorMethod(clazz, propertyKey));

        for(const methodName of methods) {
            const descriptor:any = Object.getOwnPropertyDescriptor(clazz.prototype, methodName);
            const originMethod = descriptor!.value;

            switch (descriptor!.value.constructor.name) {
                case "AsyncFunction" : {
                    descriptor!.value = async function (...args:any) {
                        const startTime = Date.now();
                        const result = await originMethod.apply(this, args);
                        const endTime = Date.now();
                        console.log(methodName, args, endTime - startTime);
                        return result;
                    };
                } break;
                default : {
                    descriptor!.value = function (...args:any) {
                        const startTime = Date.now();
                        const result = originMethod.apply(this, args);
                        const endTime = Date.now();
                        console.log(methodName, args, endTime - startTime);
                        return result;
                    };
                }
            }
            Object.defineProperty(clazz.prototype, methodName, descriptor!);
        }
        return clazz;
    };
}

function isNonConstructorMethod(target:any, propertyKey:any) {
    const propertyValue = target.prototype[propertyKey];
    const isMethod = propertyValue instanceof Function;
    return isMethod && propertyKey !== 'constructor';
}
