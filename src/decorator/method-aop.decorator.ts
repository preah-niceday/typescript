
export function MethodAopDecorator() {
    return (target:any, propertyKey:string, descriptor: PropertyDescriptor) => {
        const originMethod = descriptor.value;

        switch (descriptor!.value.constructor.name) {
            case "AsyncFunction" : {
                descriptor!.value = async function (...args: any) {
                    console.log(`AsyncFunction - ${propertyKey} : start`);
                    const result = await originMethod.apply(this, args);
                    console.log(`AsyncFunction - ${propertyKey} : end`);
                    return result;
                }
            } break;
            default : {
                descriptor!.value = function (...args: any) {
                    console.log(`Function - ${propertyKey} : start`);
                    const result = originMethod.apply(this, args);
                    console.log(`Function - ${propertyKey} : end`);
                    return result;
                };
            }
        }
    }
}
