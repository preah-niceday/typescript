import 'reflect-metadata';
import {Expose, Type} from "class-transformer";

export namespace Elements {
  export namespace Response {
    export class Option {

      @Expose()
      id!: number;

      @Expose()
      name!: string;

      @Expose()
      sort!: number;

      @Expose()
      value!: string;
    }

    export class ElementAttribute {

      @Expose()
      attributeType!: string;

      @Expose()
      id!: number;

      @Expose()
      name!: string;

      @Expose()
      @Type(() => Elements.Response.Option)
      options!: Option[];

      @Expose()
      sort!: number;

      @Expose()
      value!: boolean|string|number|[];
    }

    export class Paint {

      @Expose()
      @Type(() => Elements.Response.ElementAttribute)
      attributes!: ElementAttribute[];

      @Expose()
      @Type(() => Elements.Response.Paint)
      children!: Paint[];

      @Expose()
      id!: number;

      @Expose()
      name!: string;

      @Expose()
      sort!: number;
    }

    export class FindOne {

      @Expose()
      @Type(() => Elements.Response.ElementAttribute)
      attributes!: ElementAttribute[];

      @Expose()
      @Type(() => Paint)

      @Expose()
      children!: Paint[];

      @Expose()
      id!: number;

      @Expose()
      name!: string;

      @Expose()
      sort!: number;
    }
  }
}
