import {Attribute} from "../decorator/attribute.decorator";
import {LocalDate, LocalDateTime, LocalTime} from "@js-joda/core";

export namespace Model {

    export class NotImportTest {

        @Attribute("시간")
        time!:LocalTime;

        @Attribute("날짜")
        date!:LocalDate;

        @Attribute("날짜시간")
        dateTime!:LocalDateTime;

        @Attribute("시간-목록", ()=> LocalTime)
        times!:LocalTime[];

        @Attribute("날짜-목록", ()=> LocalDate)
        dates!:LocalDate[];

        @Attribute("날짜시간목록", ()=> LocalDateTime)
        dateTimes!:LocalDateTime[];
    }
}
