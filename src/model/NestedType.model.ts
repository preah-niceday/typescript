import {Attribute} from "../decorator/attribute.decorator";
import {IsBoolean, IsNotEmpty, IsString} from "class-validator";
import {ComponentCode} from "./enumerate/ComponentCode";
import {MakeCode} from "./enumerate/MakeCode";
import {DateFormatter} from "@/decorator/date-formatter.decorator";
import {LocalDate, LocalDateTime} from "@js-joda/core";

export namespace Model {

    export namespace Response {

        export class Target {

            @Attribute("이름")
            name!: string;

            @Attribute("노출여부")
            displayYn!: boolean;
        }

        export class FindAll {

            @Attribute("이름")
            name!: string;

            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("대상", () => Model.Response.Target)
            target!: Model.Response.Target;

            @Attribute("대상-목록", () => Model.Response.Target)
            targets!: Model.Response.Target[];

            @Attribute("컴포넌트코드")
            componentCode!: ComponentCode;

            @Attribute("컴포넌트코드-목록", () => ComponentCode)
            componentCodes!: ComponentCode[];
        }

        export class FindOne {

            @IsString() @IsNotEmpty()
            @Attribute("이름")
            name!: string;

            @IsBoolean() @IsNotEmpty()
            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("URL")
            url!: string;

            @Attribute("키워드")
            keyword!: string;

            @Attribute("설명")
            description!: string;

            @Attribute("일련번호")
            serial!: number;

            @Attribute("샘플-일련번호배열", () => Number)
            serials!: number[];

            @Attribute("샘플-제목배열", () => String)
            titles!: string[];

            @Attribute("샘플-상태배열", () => Boolean)
            status!: boolean[];

            @Attribute("오디오-컴포넌트코드-북-제작코드")
            makeCode!: MakeCode;

            @Attribute("오디오-컴포넌트코드-북-제작코드", () => MakeCode)
            makeCodes!: MakeCode[];
        }
    }

    export class Account {

        @Attribute('등록일시')
        createdAt!: LocalDateTime;

        @Attribute('수정일시')
        updatedAt!: LocalDateTime;
    }

    export namespace Request {

        export class Image {

            @Attribute("이름")
            name!: string;

            @Attribute("노출여부")
            displayYn!: boolean;
        }

        export class ImagePool {

            @Attribute("이름")
            name!: string;

            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("이미지-목록", () => Model.Request.Image)
            images!: Model.Request.Image[];
        }

        export class Body {

            @IsString() @IsNotEmpty()
            @Attribute("이름")
            name!: string;

            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("컴포넌트코드")
            componentCode!: ComponentCode;

            @Attribute("묶음이미지", ()=> Model.Request.ImagePool)
            imagePool!: Model.Request.ImagePool;
        }

        export class Page {

            @Attribute("이름")
            name!: string;

            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("바디-목록", () => Model.Request.Body)
            bodies!: Model.Request.Body[];
        }

        export class Template {

            @Attribute("이름")
            name!: string;

            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("시작일시")
            startDt!: LocalDate;

            @Attribute("종료일시")
            endDt!: LocalDate;

            @Attribute("페이지-목록", () => Model.Request.Page)
            pages!: Model.Request.Page[];
        }


        export class Version {

            @Attribute("이름")
            name!: string;

            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("시작일시")
            startDt!: LocalDateTime;

            @Attribute("종료일시")
            endDt!: LocalDateTime;
        }

        // export class Add {
        export class Add extends Model.Account {

            @IsString() @IsNotEmpty()
            @Attribute("이름")
            name!: string;

            @IsBoolean() @IsNotEmpty()
            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("URL")
            url!: string;

            @Attribute("키워드")
            keyword!: string;

            @Attribute("설명")
            description!: string;

            @Attribute("일련번호")
            serial!: number;

            @Attribute("버전-목록", () => Model.Request.Version)
            versions!: Model.Request.Version[];

            @Attribute("템플릿-목록", () => Model.Request.Template)
            templates!: Model.Request.Template[];

            @Attribute("샘플-일련번호배열", () => Number)
            serials!: number[];

            @Attribute("샘플-제목배열", () => String)
            titles!: string[];

            @Attribute("샘플-상태배열", () => Boolean)
            status!: boolean[];

            @Attribute("오디오-컴포넌트코드-북-제작코드")
            makeCode!: MakeCode;

            @Attribute("오디오-컴포넌트코드-북-제작코드", () => MakeCode)
            makeCodes!: MakeCode[];

            @Attribute("이미지", () => Model.Request.Image)
            image!:Model.Request.Image;

            @Attribute("이미지-목록", () => Model.Request.Image)
            images!:Model.Request.Image[];

            @Attribute("시작일자")
            startAt!:LocalDate;

            @Attribute("시작일자-목록", ()=> LocalDate)
            startAts!:LocalDate[];
        }

        export class Modify {

            @IsString() @IsNotEmpty()
            @Attribute("이름")
            name!: string;

            @IsBoolean() @IsNotEmpty()
            @Attribute("노출여부")
            displayYn!: boolean;

            @Attribute("URL")
            url!: string;

            @Attribute("키워드")
            keyword!: string;

            @Attribute("설명")
            description!: string;

            @Attribute("일련번호")
            serial!: number;

            @Attribute("버전-목록", () => Model.Request.Version)
            versions!: Model.Request.Version[];

            @Attribute("템플릿-목록", () => Model.Request.Template)
            templates!: Model.Request.Template[];

            @Attribute("샘플-일련번호배열", () => Number)
            serials!: number[];

            @Attribute("샘플-제목배열", () => String)
            titles!: string[];

            @Attribute("샘플-상태배열", () => Boolean)
            status!: boolean[];

            @Attribute("오디오-컴포넌트코드-북-제작코드")
            makeCode!: MakeCode;

            @Attribute("오디오-컴포넌트코드-북-제작코드", () => MakeCode)
            makeCodes!: MakeCode[];

            @Attribute("이미지", () => Model.Request.Image)
            image!:Model.Request.Image;

            @Attribute("이미지-목록", () => Model.Request.Image)
            images!:Model.Request.Image[];

            @DateFormatter("yyyyMMdd")
            @Attribute("시작일자")
            startAt!:LocalDate;

            @DateFormatter("yyyyMMdd")
            @Attribute("시작일자-목록", ()=> LocalDate)
            startAts!:LocalDate[];

            @DateFormatter("yyyyMMddhhmmss")
            @Attribute("현재시간")
            currentAt!:LocalDateTime;

            @DateFormatter("yyyyMMddhhmmss")
            @Attribute("현재시간-목록", () => LocalDateTime)
            currentAts!:LocalDateTime[];
        }
    }

    export class Date {

        @Attribute("시간")
        time!: LocalDateTime;

        @Attribute("시간-목록", () => LocalDateTime)
        times!: LocalDateTime[];
    }

    export class Enum {

        @Attribute("제작코드")
        makeCode!: MakeCode;

        @Attribute("컴포넌트코드")
        componentCode!: ComponentCode;
    }

    export class Sample {

        @Attribute("시간")
        time!: LocalDateTime;

        @Attribute("시간", () => LocalDateTime)
        times!: LocalDateTime[];

        @Attribute("회사구분")
        companyCode!:ComponentCode;

        @Attribute("회사구분", () => ComponentCode)
        companyCodes!:ComponentCode[];
    }
}
