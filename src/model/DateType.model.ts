import {Attribute} from "../decorator/attribute.decorator";
import {NLocalTime} from "@/model/transform/n-local-time";
import {NLocalDate} from "@/model/transform/n-local-date";
import {NLocalDateTime} from "@/model/transform/n-local-date-time";
import {DateFormatter} from "../decorator/date-formatter.decorator";
import {LocalDate, LocalDateTime, LocalTime} from "@js-joda/core";

export namespace Model {

    export class Date {

        @Attribute("시간")
        time!:LocalTime;

        @Attribute("날짜")
        date!:LocalDate;

        @Attribute("날짜시간")
        dateTime!:LocalDateTime;

        @Attribute("시간-목록", ()=> LocalTime)
        times!:LocalTime[];

        @Attribute("날짜-목록", ()=> LocalDate)
        dates!:LocalDate[];

        @Attribute("날짜시간목록", ()=> LocalDateTime)
        dateTimes!:LocalDateTime[];
    }

    export class DatePattern {

        @DateFormatter("HHmm")
        @Attribute("시간")
        time!:LocalTime;

        @DateFormatter("yyyyMMdd")
        @Attribute("날짜")
        date!:LocalDate;

        @DateFormatter("yyyyMMddHHmmss")
        @Attribute("날짜시간")
        dateTime!:LocalDateTime;

        @DateFormatter("HHmm")
        @Attribute("시간-목록", ()=> LocalTime)
        times!:LocalTime[];

        @DateFormatter("yyyyMMdd")
        @Attribute("날짜-목록", ()=> LocalDate)
        dates!:LocalDate[];

        @DateFormatter("yyyyMMddHHmmss")
        @Attribute("날짜시간목록", ()=> LocalDateTime)
        dateTimes!:LocalDateTime[];
    }
}
