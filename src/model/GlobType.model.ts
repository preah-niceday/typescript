import {IsBoolean, IsNotEmpty, IsString} from "class-validator";
import {ComponentCode} from "@/model/enumerate/ComponentCode";
import {MakeCode} from "@/model/enumerate/MakeCode";
import {DescriptionDecorator} from "@/decorator/description.decorator";
import {Expose, Type} from "class-transformer";
import {LocalDate, LocalDateTime} from "@js-joda/core";

export namespace Model {

    export namespace Response {

        export class Target {

            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;
        }

        export class FindAll {

            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;

            @DescriptionDecorator("대상")
            @Expose()
            @Type(()=> Target)
            target!: Model.Response.Target;

            @DescriptionDecorator("대상-목록")
            @Expose()
            @Type(()=> Target)
            targets!: Model.Response.Target[];

            @DescriptionDecorator("컴포넌트코드")
            @Expose()
            componentCode!: ComponentCode;

            @DescriptionDecorator("컴포넌트코드-목록")
            @Expose()
            @Type(()=> ComponentCode)
            componentCodes!: ComponentCode[];
        }
    }

    export class Account {

        @DescriptionDecorator('등록일시')
        @Expose()
        createdAt!: LocalDateTime;

        @DescriptionDecorator('수정일시')
        @Expose()
        updatedAt!: LocalDateTime;
    }

    export namespace Request {

        export class Image {

            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;
        }

        export class ImagePool {

            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;

            @DescriptionDecorator("이미지-목록")
            @Expose()
            @Type(() => Image)
            images!: Model.Request.Image[];
        }

        export class Body {

            @IsString() @IsNotEmpty()
            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;

            @DescriptionDecorator("컴포넌트코드")
            @Expose()
            componentCode!: ComponentCode;

            @DescriptionDecorator("묶음이미지")
            @Expose()
            @Type(() => ImagePool)
            imagePool!: Model.Request.ImagePool;
        }

        export class Page {

            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;

            @DescriptionDecorator("바디-목록")
            @Expose()
            @Type(() => Body)
            bodies!: Model.Request.Body[];
        }

        export class Template {

            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;

            @DescriptionDecorator("시작일시")
            @Expose()
            startDt!: LocalDate;

            @DescriptionDecorator("종료일시")
            @Expose()
            endDt!: LocalDate;

            @DescriptionDecorator("페이지-목록")
            @Expose()
            @Type(()=> Page)
            pages!: Model.Request.Page[];
        }


        export class Version {

            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;

            @DescriptionDecorator("시작일시")
            @Expose()
            startDt!: LocalDateTime;

            @DescriptionDecorator("종료일시")
            @Expose()
            endDt!: LocalDateTime;
        }

        export class Add extends Account {

            @IsString() @IsNotEmpty()
            @DescriptionDecorator("이름")
            @Expose()
            name!: string;

            @IsBoolean() @IsNotEmpty()
            @DescriptionDecorator("노출여부")
            @Expose()
            displayYn!: boolean;

            @DescriptionDecorator("URL")
            @Expose()
            url!: string;

            @DescriptionDecorator("키워드")
            @Expose()
            keyword!: string;

            @DescriptionDecorator("설명")
            @Expose()
            description!: string;

            @DescriptionDecorator("버전-목록")
            @Expose()
            @Type(() => Version)
            versions!: Model.Request.Version[];

            @DescriptionDecorator("템플릿-목록")
            @Expose()
            @Type(() => Template)
            templates!: Model.Request.Template[];

            @DescriptionDecorator("샘플-일련번호배열")
            @Expose()
            @Type(() => Number)
            serials!: number[];

            @DescriptionDecorator("샘플-제목배열")
            @Expose()
            @Type(() => String)
            titles!: string[];

            @DescriptionDecorator("샘플-상태배열")
            @Expose()
            @Type(() => Boolean)
            status!: boolean[];

            @DescriptionDecorator("오디오-컴포넌트코드-북-제작코드")
            @Expose()
            makeCode!: MakeCode;

            @DescriptionDecorator("이미지")
            @Expose()
            @Type(() => Image)
            image!:Image;

            @DescriptionDecorator("이미지-목록")
            @Expose()
            @Type(() => Image)
            images!:Image[];
        }
    }

    export class Date {

        @DescriptionDecorator("시간")
        @Expose()
        time!: LocalDateTime;

        @DescriptionDecorator("시간-목록")
        @Expose()
        times!: LocalDateTime;
    }

    export class Enum {

        @DescriptionDecorator("제작코드")
        @Expose()
        makeCode!: MakeCode;

        @DescriptionDecorator("컴포넌트코드")
        @Expose()
        componentCode!: ComponentCode;
    }
}
