import {Attribute} from "../../decorator/attribute.decorator";
import {IsBoolean, IsNotEmpty, IsString} from 'class-validator';

import {Enum} from "../../enum/Enum";
import {Enumerate} from "../../enumerate/Enumerate";

export namespace Model {

  export namespace Response {

    export class Target {

      @Attribute("이름")
      name!: string;

      @Attribute("노출여부")
      displayYn!: boolean;
    }

    export class FindAll {

      @Attribute("이름")
      name!: string;

      @Attribute("노출여부")
      displayYn!: boolean;

      @Attribute("대상")
      target!: Model.Response.Target;

      @Attribute("대상-목록", ()=> Model.Response.Target)
      targets!: Model.Response.Target[];

      @Attribute("컴포넌트코드", ()=> Enum.COMPONENT_CODE)
      componentCode!: Enum.COMPONENT_CODE;

      @Attribute("컴포넌트코드-목록", ()=> Enum.COMPONENT_CODE)
      componentCodes!: Enum.COMPONENT_CODE[];
    }
  }

  export class Account {

    @Attribute('등록일시')
    createdAt!: Date;

    @Attribute('수정일시')
    updatedAt!: Date;
  }

  export namespace Request {

    export class Image {

      @Attribute("이름")
      name!: string;

      @Attribute("노출여부")
      displayYn!: boolean;
    }

    export class ImagePool {

      @Attribute("이름")
      name!: string;

      @Attribute("노출여부")
      displayYn!: boolean;

      @Attribute("이미지-목록", ()=> Model.Request.Image)
      images!: Model.Request.Image[];
    }

    export class Body {

      @IsString() @IsNotEmpty()
      @Attribute("이름")
      name!: string;

      @Attribute("노출여부")
      displayYn!: boolean;

      @Attribute("컴포넌트코드", ()=> Enum.COMPONENT_CODE)
      componentCode!: Enum.COMPONENT_CODE;

      @Attribute("묶음이미지", ()=> Model.Request.ImagePool)
      imagePool!: Model.Request.ImagePool;
    }

    export class Page {

      @Attribute("이름")
      name!: string;

      @Attribute("노출여부")
      displayYn!: boolean;

      @Attribute("바디-목록", ()=> Model.Request.Body)
      bodies!: Model.Request.Body[];
    }

    export class Template {

      @Attribute("이름")
      name!: string;

      @Attribute("노출여부")
      displayYn!: boolean;

      @Attribute("시작일시")
      startDt!: Date;

      @Attribute("종료일시")
      endDt!: Date;

      @Attribute("페이지-목록", ()=> Model.Request.Page)
      pages!: Model.Request.Page[];
    }


    export class Version {

      @Attribute("이름")
      name!: string;

      @Attribute("노출여부")
      displayYn!: boolean;

      @Attribute("시작일시")
      startDt!: Date;

      @Attribute("종료일시")
      endDt!: Date;
    }

    export class Add extends Account {

      @IsString() @IsNotEmpty()
      @Attribute("이름")
      name!: string;

      @IsBoolean() @IsNotEmpty()
      @Attribute("노출여부")
      displayYn!: boolean;

      @Attribute("URL")
      url!: string;

      @Attribute("키워드")
      keyword!: string;

      @Attribute("설명")
      description!: string;

      @Attribute("버전-목록", ()=> Model.Request.Version)
      versions!: Model.Request.Version[];

      @Attribute("템플릿-목록", ()=> Model.Request.Template)
      templates!: Model.Request.Template[];

      @Attribute("샘플-일련번호배열")
      serials!: number[];

      @Attribute("샘플-제목배열")
      titles!: string[];

      @Attribute("샘플-상태배열")
      status!: boolean[];

      @Attribute("오디오-컴포넌트코드-북-제작코드", ()=> Enumerate.Audio.Component.Book.MakeCode)
      makeCode!: Enumerate.Audio.Component.Book.MakeCode;

      @Attribute("이미지", ()=> Image)
      image!:Model.Request.Image;

      @Attribute("이미지-목록", ()=> Image)
      images!:Model.Request.Image[];
    }
  }
}
