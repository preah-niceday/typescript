import {Attribute} from "../decorator/attribute.decorator";
import {ComponentCode} from "./enumerate/ComponentCode";

export namespace Model {
    export class Enum {

        @Attribute("컴포넌트코드")
        componentCode!:ComponentCode;

        @Attribute("컴포넌트코드-목록", () => ComponentCode)
        componentCodes!:ComponentCode[];
    }
}
