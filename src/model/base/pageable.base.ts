import 'reflect-metadata';
import {Expose} from 'class-transformer';

export namespace Pageable {
  export namespace Request {
    export class Search {
      @Expose()
      number!: number;

      @Expose()
      size!: number;

      constructor(options?: Options) {
        this.number = 0;
        this.size = 10;

        if (!!options) {
          if (!!options.number) {
            this.number = options.number;
          }

          if (!!options.size) {
            this.size = options.size;
          }
        }
      }
    }

    export class Options {
      @Expose()
      number?: number;

      @Expose()
      size?: number;
    }
  }

  export namespace Response {
    export class Page<T> {
      @Expose()
      number!: number;

      @Expose()
      size!: number;

      @Expose()
      total!: number;

      @Expose()
      contents!: T[];
    }
  }
}
