export class Pattern {
    name!   :string;
    pattern!:string;

    constructor(name:string, pattern:string) {
        this.name    = name;
        this.pattern = pattern;
    }
}
