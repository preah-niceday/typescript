import {DateTimeFormatter, LocalTime} from "@js-joda/core";
import {TransformFnParams} from "class-transformer/types/interfaces";
import _ from "lodash";
import {Pattern} from "../base/pattern.base";


export class NLocalTime {
    static transformClass(params: TransformFnParams, constructor:any) {
        console.log("toClass");
        if(_.isUndefined(params.value)) return;
        const patterns = Reflect.getMetadata("_patterns", constructor)?.filter((pattern:Pattern) => _.isEqual(pattern.name, params.key));
        switch (params.value.constructor.name) {
            case "String" : return _.isEmpty(patterns) ? LocalTime.parse(params.value) : LocalTime.parse(params.value, DateTimeFormatter.ofPattern(patterns[0].pattern));
            default       : return params.obj[params.key];
        }
    }

    static transformClassArray(params: TransformFnParams, constructor:any) {
        if(_.isUndefined(params.value) || !_.isArray(params.value)) return;
        const values = [];
        const patterns = Reflect.getMetadata("_patterns", constructor)?.filter((pattern:Pattern) => _.isEqual(pattern.name, params.key));
        for(const item of params.value) {
            switch (item.constructor.name) {
                case "String" : values.push(_.isEmpty(patterns) ? LocalTime.parse(item) : LocalTime.parse(item, DateTimeFormatter.ofPattern(patterns[0].pattern))); break;
                default       : values.push(params.obj[params.key][params.value.indexOf(item)]); break;
            }
        }
        return values;
    }

    static transformPlant(params: TransformFnParams, constructor:any) {
        console.log("toPlant");
        if(_.isUndefined(params.value)) return;
        const patterns = Reflect.getMetadata("_patterns", constructor)?.filter((pattern:Pattern) => _.isEqual(pattern.name, params.key));
        switch (params.value.constructor.name) {
            case "String" : return params.value;
            default       : return params.obj[params.key].format(_.isEmpty(patterns) ? DateTimeFormatter.ISO_LOCAL_TIME : DateTimeFormatter.ofPattern(patterns[0].pattern));
        }
    }

    static transformPlantArray(params: TransformFnParams, constructor:any) {
        if(_.isUndefined(params.value) || !_.isArray(params.value)) return;
        const values = [];
        const patterns = Reflect.getMetadata("_patterns", constructor)?.filter((pattern:Pattern) => _.isEqual(pattern.name, params.key));
        for(const item of params.value) {
            switch (item.constructor.name) {
                case "String" : values.push(item); break;
                default       : values.push(params.obj[params.key][params.value.indexOf(item)].format(_.isEmpty(patterns) ? DateTimeFormatter.ISO_LOCAL_TIME : DateTimeFormatter.ofPattern(patterns[0].pattern))); break;
            }
        }
        return values;
    }
}