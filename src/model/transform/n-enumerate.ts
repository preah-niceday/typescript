import {TransformFnParams} from "class-transformer/types/interfaces";
import _ from "lodash";

export class NEnumerate {

    private readonly _value!:string;
    private readonly _name:string;

    constructor(value:string, name:string) {
        this._value = value;
        this._name = name;
    }

    get value() {return this._value;}
    get name()  {return this._name;}

    static instance(name:string) {
        return new NEnumerate(name, name);
    }

    static transformClass(params: TransformFnParams, designType:any) {
        if(_.isUndefined(params.value)) return;
        switch (params.value.constructor.name) {
            case "String" : return designType[params.value];
            default       : return params.obj[params.key];
        }
    }

    static transformClassArray(params: TransformFnParams, designType:any) {
        if(_.isUndefined(params.value) || !_.isArray(params.value)) return;
        const values = [];
        for(const item of params.value) {
            switch (item.constructor.name) {
                case "String" : values.push(designType()[item]); break;
                default       : values.push(params.obj[params.key][params.value.indexOf(item)]); break;
            }
        }
        return values;
    }

    static transformPlant(params: TransformFnParams, designType:any) {
        if(_.isUndefined(params.value)) return;
        debugger
        switch (params.value.constructor.name) {
            case "String" : return params.value;
            default       : return params.obj[params.key].value;
        }
    }

    static transformPlantArray(params: TransformFnParams, designType:any) {
        if(_.isUndefined(params.value) || !_.isArray(params.value)) return;
        const values = [];
        for(const item of params.value) {
            switch (item.constructor.name) {
                case "String" : values.push(item.value); break;
                default       : values.push(params.obj[params.key][params.value.indexOf(item)].value); break;
            }
        }
        return values;
    }
}
