import {NEnumerate} from "@/model/transform/n-enumerate";
import {Enumerate} from "../../decorator/enumerate.decorator";

@Enumerate
export class LanguageCode extends NEnumerate {
    static readonly KOREA   = NEnumerate.instance('한국어')
    static readonly ENGLISH = NEnumerate.instance('영어')
    static readonly OTHER   = NEnumerate.instance('기타')
}