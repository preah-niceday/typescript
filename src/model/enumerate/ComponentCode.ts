import {NEnumerate} from "@/model/transform/n-enumerate";
import {Enumerate} from "../../decorator/enumerate.decorator";

@Enumerate
export class ComponentCode extends NEnumerate {
    static readonly TEXT  = NEnumerate.instance('텍스트')
    static readonly IMAGE = NEnumerate.instance('이미지')
    static readonly BOARD = NEnumerate.instance('게시판')
}