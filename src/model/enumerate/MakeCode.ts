import {NEnumerate} from "@/model/transform/n-enumerate";
import {Enumerate} from "../../decorator/enumerate.decorator";

@Enumerate
export class MakeCode extends NEnumerate {
    static readonly DIGEST   = NEnumerate.instance('요약형')
    static readonly COMPLETE = NEnumerate.instance('완독형')
    static readonly EXTRACT  = NEnumerate.instance('발췌형')
}
